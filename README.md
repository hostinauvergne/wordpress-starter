# Wordpress starter repository

A Wordpress base project that is built on docker and can build
image on Gitlab.

# How to use it ?

Clone this repository into an empty directory project :

```bash
mkdir myproject
cd myproject
git clone git@gitlab.com:hostinauvergne/wordpress-starter.git .
```

Change origin :

```bash
git remote rm origin
git remote add origin git@gitlab.com:mygroup/myproject.git
```

Edit [composer.json](/composer.json) to add your plugins and theme then commit and push 
to your project's repository.


